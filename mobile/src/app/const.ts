export const CONST = {
  create_table_statement : 'CREATE TABLE IF NOT EXISTS cache_users_local (id INT, uid TEXT, user_name TEXT,user_email TEXT,business_name TEXT,paid INT, phone_number INT, catid INT , invited_by INT, lastupdated TIMESTAMP)',
  create_table_statement_tapally_contacts : 'CREATE TABLE IF NOT EXISTS cache_tapally_contacts (id TEXT, displayName TEXT,mobile TEXT,photoURL TEXT,isUser INT,mobile_formatted TEXT,uid TEXT)',
  create_table_statement_tapally_friends  : 'CREATE TABLE IF NOT EXISTS cache_tapally_friends  (uid TEXT,gpflag INT, selection INT,isBlock INT, unreadmessage TEXT, displayName TEXT,mobile TEXT,photoURL TEXT ,lastMessage_message TEXT,lastMessage_type TEXT,lastMessage_dateofmsg TEXT,groupimage TEXT,groupName TEXT, lastmsg TEXT,dateofmsg TEXT,timeofmsg TEXT,isactive INT,selectCatId TEXT,referral_type TEXT,deviceToken TEXT)',
  create_table_statement_key_val : 'CREATE TABLE IF NOT EXISTS key_val (id TEXT, key TEXT,val TEXT)',
  create_table_buddychat : 'CREATE TABLE IF NOT EXISTS cache_buddychat (uid TEXT, key TEXT,val TEXT)'
  };
