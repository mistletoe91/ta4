import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { CONST} from './const';
import { SQLite, SQLiteObject   } from '@ionic-native/sqlite/ngx';
import { TpstorageProvider } from '../providers/tpstorage/tpstorage';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  pages: Array<{title: string, component: any}>;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private sqlite: SQLite,
    public tpStorageService: TpstorageProvider,
    private router: Router
  ) {
    this.initializeApp();
  }

  setRootPage (page_) {
    this.router.navigateByUrl('/tutorial');
    console.log (page_);
  }

  initializeApp() { 
    // used for an example of ngFor and navigation
    this.pages = [
        { title: 'Settings', component: "SettingPage" },
        { title: 'Business Settings', component: "RegisterbusinessPage" },
    ];

    //hardware back button
    this.backButtonEvent ();
    console.log ("2");
    this.platform.ready().then(() => {
      console.log ("3");
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      console.log ("4");
      this.checkUserAndRedirectUser ();
    });
  }//end function initaliez the app

  // active hardware back button
  backButtonEvent() {
          this.platform.backButton.subscribe(async () => {
              console.log ("Back button worked");
          });
  }//end function


   dropAllStorage (){
     console.log("DROP Everything and start fresh ");
     this.dropTable ('cache_users_local');
     this.dropTable ('cache_tapally_contacts');
     this.dropTable ('cache_tapally_friends');
     this.dropTable ('key_val');
     this.tpStorageService.clear();
   }

      dropTable(tbl) {
          this.sqlite.create({
            name: 'gr.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            console.log ("DROPPED TABLE "+tbl);
            db.executeSql( "DROP TABLE "+tbl, [])
            .then(res => {
            })
            .catch(e => {
                //Error in operation
            });//create table

          });//create database
     }//end function

    deleteFromTable() {
        this.sqlite.create({
          name: 'gr.db',
          location: 'default'
        }).then((db: SQLiteObject) => {
          db.executeSql(CONST.create_table_statement, [])
          .then(res => {
              db.executeSql("DELETE   FROM cache_users_local", [])
              .then(res => {
              })
              .catch(e => {
                  //Error in operation
              });
          })
          .catch(e => {
              //Error in operation
          });//create table

        });//create database
   }//end function

 checkUserAndRedirectUser() {
   console.log ("checkUserAndRedirectUser");
   this.sqlite.create({
     name: 'gr.db',
     location: 'default'
   }).then((db: SQLiteObject) => {
     console.log ("CHECK2");
     db.executeSql(CONST.create_table_statement, [])
     .then(res => {
         db.executeSql("SELECT *  FROM cache_users_local", [])
         .then(res => {
             if(res.rows.length>0){
                     if(!res.rows.item(0).uid || res.rows.item(0).uid == undefined){
                         this.setRootPage ('TutorialPage');
                     }

                     console.log ("App component Trying to get userUID");
                     this.tpStorageService.getItem('userUID').then((res: any) => {
                        console.log ("??Inside this.tpStorageService.getItem");
                        if (res && res != undefined) {
                          console.log ("userUID Found in tpStorageService");
                        } else {
                          console.log ("userUID NOT Found in tpStorageService so setting to "+ res.rows.item(0).uid);
                          this.tpStorageService.setItem('userUID', res.rows.item(0).uid);
                        }
                    }).catch(e => { });
                    this.tpStorageService.getItem('newname').then((res: any) => {
                       if (res && res != undefined) {
                       } else {
                         this.tpStorageService.setItem('newname', res.rows.item(0).user_name);
                       }
                   }).catch(e => { });
                   this.tpStorageService.getItem('useremail').then((res: any) => {
                      if (res && res != undefined) {
                      } else {
                        this.tpStorageService.setItem('useremail', res.rows.item(0).useremail);
                      }
                  }).catch(e =>   { });
                     this.setRootPage ('TabsPage');
                     if(!res.rows.item(0).user_name || res.rows.item(0).user_name == undefined){
                         this.setRootPage ('DisplaynamePage');
                     }
             } else {

                     this.tpStorageService.getItem('userUID').then((userId__: any) => {
                        if(userId__){
                            //A situation when we have userId in storage but not in sqlite
                            //this.userService.saveUserIdInSqlLite (userId__);
                            this.setRootPage ('TabsPage');
                        } else {
                            console.log ("RECORD DOES NOT EXISTS");
                            this.setRootPage ('TutorialPage');
                        }
                     }).catch(e => {
                        console.log ("RECORD DOES NOT EXISTS");
                        this.setRootPage ('TutorialPage');
                     });

             }//endif
         })
         .catch(e => {
         });

     })
     .catch(e => {
     });//create table
   });//create database

 }//end fuction

}//end class
